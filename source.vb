Option Explicit

' Note : Excel Ranges and Table Object Indices start at 1 but VBA array indices start at 0. 
' If functions take and return arrays then indices are assumed to start at 0.
' if functions take and return Excel Objects then indices are assumed to start at 1.

Sub btn_generateBulk()
    ' @brief compare data from new data sheet table to data on old data sheet table and records updates
    ' in bulk upload format to the bulk upload sheet. The new data sheet table is copied to the old data sheet
    ' table upon completion.
    
    Const BULK_SHEET_NAME As String = "bulk_upload"
    Const NEW_DATA_SHEET_NAME As String = "new"
    Const OLD_DATA_SHEET_NAME As String = "old"
    Const NEW_TABLE_NAME As String = "tracker"
    Const OLD_TABLE_NAME As String = "old_data"
    Const KEY_COL_NAME As String = "Project Code"
    
    Const MAX_ROWS As Integer = 400
    
    ' variable declarations
    Dim source As Excel.ListObject      ' holds Excel Table Object for new table
    Dim target As Excel.ListObject      ' holds Excel Table Object for old table
    Dim table1() As Variant             ' holds new table as two-dimensional array for faster looping
    Dim table2() As Variant             ' holds old table as two-dimensional array for faster looping
    Dim headers() As Variant            ' contains header row names
    Dim n, m As Integer                 ' holds number of rows for table1 and table2 respectively
    Dim key_col_num1 As Integer         ' holds the column number containing the id column for table 1
    Dim key_col_num2 As Integer         ' holds the column number containing the id column for table 2
    Dim bulk_row As Integer             ' tracks current row on bulk upload sheet
    Dim i, j As Integer                 ' loop index variables
    Dim match_found As Boolean          ' tracks whether project cd match found in old data table
    Dim new_row_msg As String           ' holds message for new row found msgbox
    Dim response As Integer             ' holds user response from new row msgbox
    Dim skip_all As Boolean             ' true if user wishes to ignore all new row data
    Dim changes() As Variant            ' holds changes found for printing to bulk_upload sheet
    
    ' initialize variables
    bulk_row = 2                        ' starts at 2 to avoid overwriting header
    match_found = False
    skip_all = False
    
    ' clear bulk_upload sheet (up to MAX_ROWS) except for header
    ActiveWorkbook.Sheets(BULK_SHEET_NAME).Range("A2:CC" & (MAX_ROWS + 100)).Clear
    
    ' Set list objects
    Set source = ActiveWorkbook.Worksheets(NEW_DATA_SHEET_NAME).ListObjects(NEW_TABLE_NAME)
    Set target = ActiveWorkbook.Worksheets(OLD_DATA_SHEET_NAME).ListObjects(OLD_TABLE_NAME)
    
    ' Get index number of key column
    key_col_num1 = Application.WorksheetFunction.Match(KEY_COL_NAME, source.HeaderRowRange, 0)
    key_col_num2 = Application.WorksheetFunction.Match(KEY_COL_NAME, target.HeaderRowRange, 0)
    
    ' sort table
    Call SortTable(source, key_col_num1)
    Call SortTable(target, key_col_num2)
    
    ' get number of rows in each table
    n = source.Range.Rows.Count
    m = target.Range.Rows.Count
    
    ' assign table objects to arrays
    table1 = source.Range.Value
    table2 = target.Range.Value
    
    ' get header row for column names
    headers = SliceArrayRow(table1, 1)
    
    ' loop through rows until key column matches and compare rows for changes
    For i = 2 To n  ' loop through table1 rows skipping header
        For j = 2 To m  ' loop through table2 rows until key_col matches
            If table1(i, key_col_num1) = table2(j, key_col_num2) Then
                ' compare rows and store updates
                changes = GetChanges(SliceArrayRow(table1, i), SliceArrayRow(table2, j), headers, key_col_num1 - 1, key_col_num2 - 1)
                ' transpose two-dimensional array
                If Not changes(0, 0) = -1 And bulk_row < MAX_ROWS Then
                    changes = TransposeMatrix(changes)
                    ' print to bulk upload
                    Call ArrayFillRange(changes, BULK_SHEET_NAME, bulk_row, 1)
                    ' increment bulk upload sheet row count
                    bulk_row = bulk_row + (UBound(changes, 1) - LBound(changes, 1) + 1)
                End If
                match_found = True
                ' stop checking table 2 for match
                Exit For
            End If
        Next j
        ' check if project cd was matched to a row in second table
        If match_found = False And skip_all = False Then
            ' ask if user wants to add all milestones in row to bulk upload sheet
            new_row_msg = "A new row has been added with ID " & table1(i, key_col_num1) & ". Do you want to add all milestones for this project to Bulk Upload Sheet?"
            response = MsgBox(new_row_msg, vbYesNo + vbExclamation + vbDefaultButton2, "New Row Found")
            If response = vbYes Then
                ' get all milestone data from new row
                changes = GrabAllMilestones(SliceArrayRow(table1, i), key_col_num1 - 1, headers)
                ' print row to bulk upload sheet
                If bulk_row < MAX_ROWS Then
                    Call ArrayFillRange(changes, BULK_SHEET_NAME, bulk_row, 1)
                    bulk_row = bulk_row + (UBound(changes, 1) - LBound(changes, 1) + 1)
                End If
            Else
                response = MsgBox("Do you want to ignore all new rows?", vbYesNo + vbDefaultButton2)
                If response = vbYes Then
                    skip_all = True
                End If
            End If
        End If
        ' reset match_found for next iteration
        match_found = False
    Next i
    
    ' copy new table data to old table data
    Call CopyTable(source, target)
End Sub

Sub SortTable(ByRef table As Excel.ListObject, ByVal col_index As Integer)
    '@brief sorts table in Ascending Order by given column
    '@param table         - the table to be sorted
    '@param col_index     - the column number of the column to sort by

    With table
        ' clear filters
        .AutoFilter.ShowAllData
        ' clear any applied sorts so new sort can be added
        .Sort.SortFields.Clear
        ' add sort with options
        .Sort.SortFields.Add Key:=table.Range.Columns(col_index), _
            SortOn:=xlSortOnValues, _
            Order:=xlAscending
        ' table has headers
        .Sort.Header = xlYes
        ' apply sort method
        .Sort.Apply
    End With
    
End Sub

Function SliceArrayRow(ByRef arr2D() As Variant, ByVal row_index As Integer) As Variant()
    ' @brief Returns row from matrix table at given row number - MUST BE TWO DIMENSIONAL ARRAY
    ' @param arr2D()   - the two-dimensional array to slice
    ' @param row_index - the row number to slice
    
    Dim temp() As Variant   ' temp array to hold row data returned
    Dim i As Integer        ' loop control variable
    
    ReDim temp(UBound(arr2D, 2))
    
    For i = 1 To UBound(arr2D, 2)
        temp(i - 1) = arr2D(row_index, i)
    Next i
    
    SliceArrayRow = temp
End Function

Sub ArrayFillRange(ByRef arr() As Variant, ByVal sheet_name As String, ByVal start_row As Integer, ByVal start_col As Integer)
    ' @brief outputs array to Excel Range - requires that array start at index 0
    ' @param arr()         - array to print
    ' @param sheet_name    - sheet name to print to
    ' @param start_row     - row to start printing at
    ' @param start_col     - column to start printing at
    
    Dim num_rows, num_cols As Integer       ' holds number of rows and columns in array
    Dim range_select As Range               ' holds Excel range to print to
    
    ' Get the dimensions
    num_rows = UBound(arr, 1)
    num_cols = UBound(arr, 2)

    ' Set worksheet range
    Set range_select = ThisWorkbook.Worksheets(sheet_name).Range(Cells(start_row, start_col), _
    Cells(num_rows + start_row, num_cols + start_col))
    
    ' Transfer array to worksheet
    range_select.Value = arr
    
End Sub

Sub CopyTable(ByRef source_tbl As Excel.ListObject, ByRef target_tbl As Excel.ListObject)
    ' @brief copies data from one table to anothoer table - tables must not be empty!!
    ' @param source_tbl     - excel ListObject of the table to be copied
    ' @param target_tbl     - excel ListObject of the table to copy to
    
    With target_tbl
        ' clear and resize table for new data
        If .DataBodyRange.Rows.Count <> source_tbl.DataBodyRange.Rows.Count Then
            .DataBodyRange.Cells.Clear
            .Resize .Range.Cells(1).Resize(source_tbl.HeaderRowRange.Rows.Count + _
                source_tbl.DataBodyRange.Rows.Count, source_tbl.Range.Columns.Count)
        End If
        
        ' copy
        source_tbl.DataBodyRange.Copy Destination:=.DataBodyRange.Cells(1)
        source_tbl.HeaderRowRange.Copy Destination:=.HeaderRowRange.Cells(1)
    End With
    
End Sub

Private Function FindMilestoneStartCol(ByRef headers As Variant, Optional ByVal start_index As Integer = 1) As Integer
' @brief returns column index for start of milestone data based on | dt | sts | dt | sts | ... | structure
' if no match found returns -1
' @param headers                   - array containing headers
' @param start_index (optional)    - index to start search at

    Dim n As Integer        ' length of header array
    Dim i As Integer        ' loop control variable
    
    n = UBound(headers)
        
    For i = start_index To n
        If InStr(headers(i), "Sts") <> 0 Then ' first sts column found
            FindMilestoneStartCol = i - 1 ' assumes date column comes before sts column
            Exit Function
        End If
    Next i
    
    ' if no match found in any column return -1
    FindMilestoneStartCol = -1
End Function

Private Function GrabAllMilestones(ByRef ms_row() As Variant, ByVal proj_id_index As Integer, ByRef headers() As Variant) As Variant()
    ' @brief Returns all milestone data in given row as an array in bulk upload format
    ' @param ms_row()              - the variant array containing row data
    ' @param proj_id_index         - the column index for the project cd column
    ' @param headers()             - variant array containing header names
    ' @param start_index (optional)- column to start search at
    ' @return returns variant array containing all milestones in bulk upload format: [proj_cd, task_name, task_dt, task_sts]
    
    Dim milestones() As Variant
    Dim i, n As Integer
    Dim row_num As Integer
    Dim start_index As Integer
    
    row_num = 0
    
    start_index = FindMilestoneStartCol(headers, proj_id_index)
    
    If (UBound(ms_row) - start_index) Mod 2 = 0 Then
        n = ((UBound(ms_row) - start_index) / 2)
    Else
        ' ignore last column since not in date | status format
        n = ((UBound(ms_row) - start_index - 1) / 2)
    End If
    
    ReDim milestones(n, 3)
    
    ' loop through row to grab each milestone date and sts
    For i = start_index To (UBound(ms_row) - 1)
        milestones(row_num, 0) = ms_row(proj_id_index)
        milestones(row_num, 1) = headers(i)
        milestones(row_num, 2) = ms_row(i)
        milestones(row_num, 3) = ms_row(i + 1)
        
        row_num = row_num + 1
        i = i + 1
    Next i
    
    GrabAllMilestones = milestones
End Function

Function GetChanges(ByRef rowA() As Variant, ByRef rowB() As Variant, ByRef headers() As Variant, _
ByVal key_col_A As Integer, ByVal key_col_B As Integer) As Variant()
    ' @brief searches for changes to data beginning at index columns and returns array in TRANSPOSED bulk upload format
    ' columns must match beginning at key column for each row
    ' transposed because ReDim Preserve only allows changes to last dimension
    ' @param rowA()            - row to compare
    ' @param rowB()            - row to compare
    ' @param headers()         - header names corresponding to rows
    ' @param key_col_A         - index column number for rowA, assumes indices start at zero
    ' @param key_col_B         - index column number for rowB, assumes indices start at zero
    
    Dim changes() As Variant
    Dim i, j, row, n As Integer
    Dim num_cols As Integer
    
    row = 2
    j = key_col_B
    n = 0
    
    If UBound(rowA) <= UBound(rowB) Then
        num_cols = UBound(rowA)
    Else
        num_cols = UBound(rowB)
    End If
    
    For i = key_col_A To num_cols
        If rowA(i) <> rowB(j) Then
            ReDim Preserve changes(3, n)
            changes(0, n) = rowA(key_col_A)
            If IsDate(rowA(i)) Then
                changes(1, n) = headers(i)
                changes(2, n) = rowA(i)
                changes(3, n) = rowA(i + 1)
                i = i + 1 ' skip sts field check
                j = j + 1
            Else
                changes(1, n) = headers(i - 1)
                changes(2, n) = rowA(i - 1)
                changes(3, n) = rowA(i)
            End If
            row = row + 1
            n = n + 1
        End If
        j = j + 1
    Next i
    
    If n = 0 Then
        ReDim changes(0, 0)
        changes(0, 0) = -1
    End If
    
    GetChanges = changes
End Function

Private Function TransposeMatrix(ByRef matrix() As Variant) As Variant()
    'transposes rows and columns of two dimensional array
    Dim temp() As Variant
    Dim i, j As Integer
    
    ReDim temp(UBound(matrix, 2), UBound(matrix, 1))
        
    For i = 0 To UBound(matrix, 2)
        For j = 0 To UBound(matrix, 1)
            temp(i, j) = matrix(j, i)
        Next j
    Next i
    
    TransposeMatrix = temp
End Function
